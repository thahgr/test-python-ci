FROM registry.ubitech.eu/public-group/ubitech-public-registry/python:3.8

COPY requirements.txt ./

RUN pip install -r requirements.txt

